import React, { Component } from 'react'
import axios from 'axios';

export default class Fetch extends Component {

    state = {
        posts : []
    }
    
    componentDidMount() {
        axios.get(`https://www.reddit.com/r/reactjs.json`)
            .then(res => {
                const posts = res.data.data.children.map(obj => obj.data);
                this.setState({ posts })
            });
    }

    render() {
        return (
            <div>
                <ul>
                    { this.state.posts.map(post => (
                        <li key={post.id} >{post.title}</li>
                    )) }
                </ul>
            </div>
        )
    }
}
